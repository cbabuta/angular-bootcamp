import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-page',
  templateUrl: './side-page.component.html',
  styleUrls: ['./side-page.component.scss']
})
export class SidePageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
