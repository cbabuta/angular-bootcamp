import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SidePageComponent } from './side-page/side-page.component';
import { LandingChildPageComponent } from './landing-page/landing-child-page/landing-child-page.component';
import { LandingChildWithParamPageComponent } from './landing-page/landing-child-with-param-page/landing-child-with-param-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    SidePageComponent,
    LandingChildPageComponent,
    LandingChildWithParamPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
