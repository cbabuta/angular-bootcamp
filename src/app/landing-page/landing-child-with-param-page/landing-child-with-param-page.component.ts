import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-landing-child-with-param-page',
  templateUrl: './landing-child-with-param-page.component.html',
  styleUrls: ['./landing-child-with-param-page.component.scss']
})
export class LandingChildWithParamPageComponent implements OnInit {

  id : number
  private sub: Subscription;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id'])
        this.id = +params['id']; // (+) converts string 'id' to a number
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
