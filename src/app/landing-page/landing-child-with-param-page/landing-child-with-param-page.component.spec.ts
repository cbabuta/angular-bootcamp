import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingChildWithParamPageComponent } from './landing-child-with-param-page.component';

describe('LandingChildWithParamPageComponent', () => {
  let component: LandingChildWithParamPageComponent;
  let fixture: ComponentFixture<LandingChildWithParamPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingChildWithParamPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingChildWithParamPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
