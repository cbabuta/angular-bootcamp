import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingChildPageComponent } from './landing-child-page.component';

describe('LandingChildPageComponent', () => {
  let component: LandingChildPageComponent;
  let fixture: ComponentFixture<LandingChildPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingChildPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingChildPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
