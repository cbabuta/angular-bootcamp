import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  constructor(private router: Router) {}
  ngOnInit() {
  }

  addSession() {
    window.localStorage.setItem("session","logged on");
  }

  clearSession() {
    window.localStorage.removeItem("session");
  }
}
