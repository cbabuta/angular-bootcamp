import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SidePageComponent } from './side-page/side-page.component';
import { LandingChildPageComponent } from './landing-page/landing-child-page/landing-child-page.component';
import { LandingChildWithParamPageComponent } from './landing-page/landing-child-with-param-page/landing-child-with-param-page.component';
import { AuthGuardService } from './services/auth/auth-guard.service';


const routes: Routes = [
  { path: 'landing', redirectTo: "/landing/", pathMatch:"full"},
  { path: 'landing/:id', component: LandingPageComponent,
    children: [
      { path: "", component: LandingChildPageComponent, outlet:"withoutParams" },
      { path: "", component: LandingChildWithParamPageComponent, outlet:"withParams" }
    ]
  },
  { path: 'side',  component: SidePageComponent, canActivate: [AuthGuardService]},
  { path: '**',  redirectTo:"/landing", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
